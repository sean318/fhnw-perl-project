#! /usr/bin/env perl

use v5.34;
use warnings;

use List::Util qw(shuffle);
use Term::ANSIColor;


my @stopwords;
{
    local($/) = undef; #read till EOF
    open (FILE, "./stopwords.txt");
    my $stopwords = <FILE>;
    close FILE;
    @stopwords = split("\n", $stopwords);
}
my ($stop_regex) = map qr/(?:$_)/, join "|", map qr/\b\Q$_\E\b/, @stopwords;


state $min_spacer_length = 5;


sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };




sub parse_exam {
    my $file_path = shift;

    my %question_to_answers;
    my %question_to_correct;

    my $exam_text;
    {
        local($/) = undef; #read till EOF
        open (FILE, $file_path);
        $exam_text = <FILE>;
        close FILE;
    }

    #a few normalisations to improve matching. I decided to do them here instead of for each
    #question/answer - the result is the same, but we don't need a subroutine and it's probably faster.
    #we also get better matching of the question/answer blocks for free.

    $exam_text = lc($exam_text); #match in lowercase
    $exam_text =~ s/$stop_regex//g; #remove stopwords
    $exam_text =~ s/[\r\n]+/\n/g; #normalize LF and remove empty lines
    $exam_text =~ s/[^\S\r\n]+/ /g; #collapse whitespace
    $exam_text =~ s/^[^\S\r\n]*(.*)[^\S\r\n]*$/$1/mg; #trim lines

    #end normalisations

    state $section_re = qr/(\n\s*\d+\..*?\n[_=]{50,})/s;
    state $answer_re = '(^[^\S\r\n]*\[.\].*)';

    my @exam_sections = split($section_re, $exam_text);

    foreach my $section (@exam_sections){
        if($section =~ $section_re){ #only transform question sections
            my ($question) = $section =~ /(\d+.+)/;
            $question = trim($question); #remove trailing whitespace for better matching

            my @cur_question_correct;
            my @cur_question_answers;
            

            my @answers = $section =~ /$answer_re/mg;

            foreach(@answers){
                my ($checkbox, $answer) = $_ =~ /(\[.\])\s(.*)/;
                $answer = trim($answer);

                push(@cur_question_answers, $answer);
                if($checkbox eq "[x]"){
                    push(@cur_question_correct, $answer);
                }
            }

            $question_to_answers{$question} = \@cur_question_answers;
            $question_to_correct{$question} = \@cur_question_correct;
        }
    };

    return (\%question_to_answers, \%question_to_correct);
}

my $master_file = shift(@ARGV);
my @student_files = @ARGV;

my ($master_answers_by_question, $master_correct_by_question) = &parse_exam($master_file);
my %master_answers_by_question = %$master_answers_by_question;
my %master_correct_by_question = %$master_correct_by_question;

sub analyze_warn {
    print colored(['bright_red'], shift, "\n");
}

sub analyze_student_file{
    my $student_file = shift;
    my $student_file_names_ref = shift;
    my $student_scores_ref = shift;

    my ($student_file_name) = $student_file =~ /.*\/(.*)/;
    push(@$student_file_names_ref, $student_file_name);

    say "\n\nanalyzing $student_file_name...";

    my ($student_answers_by_question, $student_correct_by_question) = &parse_exam($student_file);
    my %student_answers_by_question = %$student_answers_by_question;
    my %student_correct_by_question = %$student_correct_by_question;


    my $answer_count = 0;
    my $correct_count = 0;

    keys %master_answers_by_question; # https://stackoverflow.com/questions/3033/whats-the-safest-way-to-iterate-through-the-keys-of-a-perl-hash
    while(my($question, $answers) = each %master_answers_by_question) {
        if(!exists($student_answers_by_question{$question})){
            analyze_warn("\tMissing question: $question");
        }else{
            my @student_answers = @{$student_answers_by_question{$question}};
            my @student_correct = @{$student_correct_by_question{$question}};
            my @master_correct = @{$master_correct_by_question{$question}};

            foreach my $answer (@{$answers}){
                if(!($answer ~~ @student_answers)){
                    analyze_warn("\tMissing answer:\t\t$answer");
                }
            }
            if(@student_correct == 0){
                #no answer given
            }elsif(@student_correct == 1){
                $answer_count ++;
                if($master_correct[0] eq $student_correct[0]){
                    $correct_count ++;
                }
            }else{
                analyze_warn("\tMore than 1 answer:\t$question");
                $answer_count ++; #i'm not 100% sure if this case should count towards "any answer given" from the description.
            }
        }
    }

    my $score = sprintf("%02d/%02d", $correct_count, $answer_count);

    push(@$student_scores_ref, $score);
}

my @student_file_names;
my @student_scores;

foreach my $student_file (@student_files){
    analyze_student_file($student_file, \@student_file_names, \@student_scores);
}

say "\n\n\n";


my $max_file_name_length = 0;
foreach my $student_file_name (@student_file_names){
    if($max_file_name_length < length($student_file_name)){
        $max_file_name_length = length($student_file_name);
    }
}

my $index = 0;
foreach my $student_file_name (@student_file_names){
    my $spacer_length = $max_file_name_length - length($student_file_name) + $min_spacer_length;
    my $spacer = "." x $spacer_length;

    print $student_file_name;
    print colored(['grey7'], $spacer);
    say $student_scores[$index];

    $index ++;
}
