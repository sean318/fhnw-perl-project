#! /usr/bin/env perl

use v5.34;
use warnings;

use List::Util qw(shuffle);

my $file_path = $ARGV[0];


my $exam_text;
{
  local($/) = undef; #read till EOF
  open (FILE, $file_path);
  $exam_text = <FILE>;
  close FILE;
}
$exam_text =~ s/\[X\]/[ ]/g; #remove the "correct" markers


state $section_re = qr/(\n\s*\d+\..*?\n[_=]{50,})/s;
state $answer_re = '(^[^\S\r\n]*\[ \].*)';

my @exam_sections = split($section_re, $exam_text);

@exam_sections = map {
    my $section = $_;
    if($section =~ $section_re){ #only transform question sections
        my @answers = $section =~ /$answer_re/mg;
        @answers = shuffle(@answers);

        my $shuffled_answers = join("\n", @answers);

        $section =~ s/($answer_re\n)+/$shuffled_answers\n/m; #replace unshuffled answers in-place
    }
    $section;
} @exam_sections;

$exam_text = join ("", @exam_sections);

my ($sec,$min,$hour,$mday,$mon,$year) = localtime();
state $formatted_date = sprintf("%04d%02d%02d-%02d%02d%02d-", $year+1900, $mon+1, $mday, $hour, $min, $sec);

$file_path =~ s/(.*)(\/)(.*)/$1$2$formatted_date$3/;

open(FILE, '>', $file_path);
print FILE $exam_text;
close FILE;
